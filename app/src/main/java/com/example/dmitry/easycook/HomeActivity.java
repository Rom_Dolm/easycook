package com.example.dmitry.easycook;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.io.IOException;

public class HomeActivity extends AppCompatActivity {
    private ArrayList<String> products = new ArrayList<>();

    AutoCompleteTextView mAutoCompleteTextView;
    ListView ingredientList;
    Button mOkButton, mFindButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        
        mAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteIngredient);
        mOkButton = (Button) findViewById(R.id.buttonOK);
        mFindButton = (Button) findViewById(R.id.buttonFind);
        
        mAutoCompleteTextView.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, ""));
        
        
        ingredientList = (ListView) findViewById(R.id.ingredientList);
        final IngredientAdapter adapter = new IngredientAdapter(this, R.layout.list_item, products, mFindButton);
        ingredientList.setAdapter(adapter);
        
        mFindButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent resultIntent = new Intent(HomeActivity.this, ResultActivity.class);
                Bundle b = new Bundle();
                //data from seekbar or 0
                b.putStringArrayList("ingredients", products);

                resultIntent.putExtras(b);
                startActivity(resultIntent);
            }
        });
        
        
        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newIngredient = mAutoCompleteTextView.getText().toString();
                if (mIngredients.contains(newIngredient)) {

                    if (products.contains(newIngredient)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                        builder.setTitle("Оповещение").setMessage("Этот ингредиент уже выбран").setCancelable(true).setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        products.add(newIngredient);
                        adapter.notifyDataSetChanged();
                        try {
                            FileOutputStream fOut = openFileOutput("chosenIngr.txt", MODE_PRIVATE);
                            OutputStreamWriter osw = new OutputStreamWriter(fOut);
                            for (int i = 0; i < products.size(); i++) {
                                osw.write(products.get(i) + "\n");
                            }
                            osw.flush();
                            osw.close();
                            System.out.println("Changed");
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                        if (products.size() != 0){
                            mFindButton.setEnabled(true);
                        }
                        mAutoCompleteTextView.setText("");
                    }

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                    builder.setTitle("Оповещение").setMessage("Пожалуйста, введите ингредиент из выпадающего списка").setCancelable(true).setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

            }
        });


    }
}
