package com.example.dmitry.easycook;
import com.example.dmitry.easycook.ResultElement;


import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import java.util.ArrayList;


public class ResultActivity extends AppCompatActivity {
    ArrayList<String> products;
    int calories;
    


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            products = b.getStringArrayList("ingredients");
            calories = b.getInt("calories");
        }   
    }
}
